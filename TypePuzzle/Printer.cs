﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TypePuzzle
{
    class Printer
    {
        public void Print(IEnumerable<Property> properties)
            => Print(properties, "  - ");

        private void Print(IEnumerable<Property> properties, string prefix)
        {
            foreach (var property in properties ?? Enumerable.Empty<Property>())
            {
                Console.Write(prefix);
                Console.WriteLine(property.Name);

                Print(property.Properties, $"  {prefix}");
            }
        }
    }
}
