﻿using System.Collections.Generic;

namespace TypePuzzle
{
    public class Property
    {
        public string Name { get; set; }
        public IEnumerable<Property> Properties { get; set; }
    }
}
