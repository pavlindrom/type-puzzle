﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TypePuzzle
{
    class Parser
    {
        private static readonly Regex ComplexType = new Regex(@"\([^()]+\)", RegexOptions.Compiled);
        private static readonly Regex TypeDefinition = new Regex(@"^(?<name>\w+)(#(?<id>\d*))?$", RegexOptions.Compiled | RegexOptions.ExplicitCapture);

        private Random _random = new Random();
        private Dictionary<int, IEnumerable<Property>> _definitions = new Dictionary<int, IEnumerable<Property>>();

        public IEnumerable<Property> Parse(string code)
        {
            var match = ComplexType.Match(code);
            if (match.Success)
            {
                var definitionId = _random.Next();
                _definitions[definitionId] = ParseSingle(match.Value);
                var simplifiedCode = $"{code[..match.Index]}#{definitionId}{code[(match.Index + match.Length)..]}";

                return Parse(simplifiedCode);
            }
            else if (int.TryParse(code[1..], out var definitionId) && _definitions.ContainsKey(definitionId))
            {
                return _definitions[definitionId];
            }

            throw new Exception("Parsing error.");
        }

        private IEnumerable<Property> ParseSingle(string code)
        {
            var result = new List<Property>();
            foreach (var property in code[1..^1].Split(", "))
            {
                var definition = TypeDefinition.Match(property);
                if (!definition.Success)
                    throw new Exception("Parsing error.");

                IEnumerable<Property> subProperties = null;
                if (int.TryParse(definition.Groups["id"].Value, out var definitionId))
                {
                    subProperties = _definitions[definitionId];
                    _definitions.Remove(definitionId);
                }

                result.Add(new Property
                {
                    Name = definition.Groups["name"].Value,
                    Properties = subProperties
                });
            }

            return result;
        }
    }
}
