﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TypePuzzle
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var code = args.FirstOrDefault();
            if (string.IsNullOrEmpty(code))
            {
                Console.WriteLine("What do you want to parse?");
                code = Console.ReadLine();
            }

            var props = Parse(code);
            Print(props);

            Console.WriteLine();
            Console.WriteLine();

            var sorted = Sort(props);
            Print(sorted);

            Console.ReadLine();
        }

        private static IEnumerable<Property> Parse(string code)
            => new Parser().Parse(code);

        private static IEnumerable<Property> Sort(IEnumerable<Property> properties)
            => properties?.OrderBy(p => p.Name)
                .Select(p => new Property
                {
                    Name = p.Name,
                    Properties = Sort(p.Properties)
                })
                .ToList();

        private static void Print(IEnumerable<Property> properties)
            => new Printer().Print(properties);
    }
}
